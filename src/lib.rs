use glob::Pattern;

#[derive(Debug)]
pub struct Config<'a> {
    pub pattern: &'a str,
    pub source: &'a str,
    pub case_sensitive: bool,
    pub trim_whitespace: bool,
    pub mode: Mode,
}

#[derive(Debug)]
pub enum Mode {
    Regex,
    Glob,
    Literal,
}

impl Mode {
    pub fn from(s: &str) -> Result<Self, &str> {
        match s {
            s if s == "r" || s == "regex" => Ok(Self::Regex),
            s if s == "g" || s == "glob" => Ok(Self::Glob),
            s if s == "l" || s == "literal" => Ok(Self::Literal),
            _ => Err("Invalid mode"),
        }
    }
}

pub fn search_literal<'a>(pattern: &str, source: &'a str, case_sensitive: bool) -> Vec<&'a str> {
    if case_sensitive {
        return source
            .lines()
            .filter(|x| x.contains(pattern))
            .collect::<Vec<&str>>();
    } else {
        let pattern = pattern.to_lowercase();
        return source
            .lines()
            .filter(|x| x.to_lowercase().contains(&pattern))
            .collect::<Vec<&str>>();
    }
}

pub fn search_glob<'a>(pattern: glob::Pattern, source: &'a str, case_sensitive: bool) -> Vec<&'a str> {
    let opts = glob::MatchOptions {
        case_sensitive: case_sensitive,
        require_literal_separator: false,
        require_literal_leading_dot: false,
    };
    source
        .lines()
        .filter(|x| pattern.matches_with(x, opts))
        .collect::<Vec<&str>>()
}

pub fn search_amalg<'a>(cfg: &'a Config) -> Result<Vec<&'a str>, String> {
    match cfg.mode {
        Mode::Literal => Ok(search_literal(cfg.pattern, cfg.source, cfg.case_sensitive)),
        Mode::Glob => {
            let ptrn = match Pattern::new(cfg.pattern) {
                Ok(pat) => Ok(search_glob(pat, cfg.source, cfg.case_sensitive)),
                Err(e) => Err(e),
            };
            match ptrn {
                Ok(val) => Ok(val),
                Err(e) => Err(format!("{:?}", e)),
            }
        }
        Mode::Regex => {
            let v: Vec<&str> = vec![];
            Ok(v)
        }
    }
}

pub fn render(v: Vec<&str>, trim: bool) {
    if trim {
        for q in v {
            println!("{}", q.trim());
        }
    } else {
        for q in v {
            println!("{}", q);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn fail() {
        assert!(1 == 0);
    }

    #[test]
    fn literal() {
        let cfg = Config {
            pattern: "NEEDLE",
            source: "\nhayhayhayhay\nhayhay\nhayNEEDLEhay\nhayhayhay\nNEEDLEhay\nhayhay\nNEEDLE",
            case_sensitive: true,
            mode: Mode::Literal,
            trim_whitespace: false,
        };
        assert_eq!(
            vec!["hayNEEDLEhay", "NEEDLEhay", "NEEDLE"],
            search_literal(cfg.pattern, cfg.source, cfg.case_sensitive)
        );
    }

    #[test]
    fn glob() {
        let cfg = Config {
            pattern: "N*DL*",
            source: "\nhayhayhayhay\nhayhay\nhayNEEDLEhay\nhayhayhay\nNEEDLEhay\nhayhay\nNEEDLE",
            case_sensitive: true,
            mode: Mode::Glob,
            trim_whitespace: false,
        };
        assert_eq!(
            vec!["NEEDLEhay", "NEEDLE"],
            search_glob(
                Pattern::new(cfg.pattern).unwrap(),
                cfg.source,
                cfg.case_sensitive
            )
        );
    }

    #[test]
    fn amalg_literal() {
        let cfg = Config {
            pattern: "NEEDLE",
            source: "\nhayhayhayhay\nhayhay\nhayNEEDLEhay\nhayhayhay\nNEEDLEhay\nhayhay\nNEEDLE",
            case_sensitive: true,
            mode: Mode::Literal,
            trim_whitespace: false,
        };
        assert_eq!(
            vec!["hayNEEDLEhay", "NEEDLEhay", "NEEDLE"],
            search_amalg(&cfg).unwrap()
        );
    }

    #[test]
    fn amalg_glob() {
        let cfg = Config {
            pattern: "N*DL*",
            source: "\nhayhayhayhay\nhayhay\nhayNEEDLEhay\nhayhayhay\nNEEDLEhay\nhayhay\nNEEDLE",
            case_sensitive: true,
            mode: Mode::Glob,
            trim_whitespace: false,
        };

        assert_eq!(search_amalg(&cfg).unwrap(), vec!["NEEDLEhay", "NEEDLE"]);
    }

    #[test]
    fn glob_no_match() {
        let cfg = Config {
            pattern: "N*DL*^^\\asc\\as####",
            source: "\nhayhayhayhay\nhayhay\nhayNEEDLEhay\nhayhayhay\nNEEDLEhay\nhayhay\nNEEDLE",
            case_sensitive: true,
            mode: Mode::Glob,
            trim_whitespace: false,
        };

        assert!(search_amalg(&cfg).unwrap() != vec!["NEEDLEhay", "NEEDLE"]);
    }

    #[test]
    fn glob_insensitive() {
        let cfg = Config {
            pattern: "n*dl*",
            source: "\nhayhayhayhay\nhayhay\nhayNEEDLEhay\nhayhayhay\nNEEDLEhay\nhayhay\nNEEDLE",
            case_sensitive: false,
            mode: Mode::Glob,
            trim_whitespace: false,
        };
        assert_eq!(
            vec!["NEEDLEhay", "NEEDLE"],
            search_glob(
                Pattern::new(cfg.pattern).unwrap(),
                cfg.source,
                cfg.case_sensitive
            )
        );
    }

    #[test]
    fn amalg_regex() {
        fail()
    }
}

/*impl Config {
    pub fn build(app: App) -> Self {
        Self {
            pattern: app.value_of("PATTERN").unwrap(),
            source: app.value_of("SOURCE").unwrap(),
            case_sensitive: app.is_present("nocase"),
            mode: Mode::Glob,
        }
    }
}*/

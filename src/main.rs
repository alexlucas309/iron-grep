#![allow(unused_imports)]
#[macro_use]
extern crate clap;
use clap::{App, Arg};
use iron_grep::*;
use std::fs;

fn main() {
    let app = App::new("Iron Grep")
        .version(crate_version!())
        .author("Copyright (c) 2020 Alexander Lucas <alexlucas309@protonmail.com>")
        .version_short("v")
        .args(&[
            Arg::with_name("PATTERN")
                .help("Sets the pattern to search for")
                .required(true),
            Arg::with_name("SOURCE")
                .help("Sets the text to search in")
                .required(true),
            Arg::with_name("ignore-case")
                .short("i")
                .long("ignore-case")
                .help("Ignores case during search with literals and globs"),
            Arg::with_name("trim-whitespace")
                .short("t")
                .help("Removes whitespace from the beginning and end of lines during output")
                .long("trim-whitespace"),
            Arg::with_name("mode")
                .short("m")
                .long("mode")
                .help("Sets method to search by. Possible values: \"regex\" (r), \"literal\" (l), \"glob\" (g)")
                .takes_value(true),
            Arg::with_name("setopt")
                .long("setopt")
                .takes_value(true)
                .help("Sets default options"),
            Arg::with_name("getopt")
                .long("getopt")
                .takes_value(true)
                .help("Prints the current value of a give option"),
            Arg::with_name("lsopts")
                .long("lsopts")
                .help("Prints availabe options"),
        ])
        .get_matches();

    //Build config object; handle errors
    //This will need to be done again when to resolve the error unification issue [#1]
    let source = app.value_of("SOURCE").unwrap();
    let source_text = fs::read_to_string(source).unwrap_or_else(|e| {
        eprintln!("Could not read {}: {}", source, e);
        std::process::exit(1);
    });
    let cfg = Config {
        pattern: { app.value_of("PATTERN").unwrap() },
        source: { &source_text },
        //source: source,
        case_sensitive: { !app.is_present("ignore-case") },
        mode: {
            match Mode::from(app.value_of("mode").unwrap_or("literal")) {
                Ok(mode) => mode,
                Err(e) => {
                    eprintln!("{}", e);
                    std::process::exit(1);
                }
            }
        },
        trim_whitespace: { app.is_present("trim-whitespace") },
    };
    //println!("{:?}", &cfg);
    render(search_amalg(&cfg).unwrap(), false); //remove unwrap for release
}
